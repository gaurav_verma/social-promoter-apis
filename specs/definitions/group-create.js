module.exports = {

    categoryId: 'string',
    categoryName: 'string',
    name: 'string',
    admin: 'string',
    link: 'string',
    typeId: 'string',
    typeName: 'string',
    imageUrl:'string',
    reportCount: 0
}
