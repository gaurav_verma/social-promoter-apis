'use strict'
const imageUploadService = require('./files')
// const response = require('../exchange/response')
const categoryService = require('../services/categories')
const linkPreview = require('link-preview-js')
// import { getLinkPreview, getPreviewFromContent } from 'link-preview-js';

// model pass in response
const createTempProductObj = async (product, count, skipCount, totalCount) => {
    var productObj = {
        products: product,
        count: count,
        skipCount: skipCount,
        totalCount: totalCount
    }

    return productObj;
}

// set method to update similarProduct
const set = async (body, req, entity, context) => {
    try {
        let data
        if (req) {

            if (body.name) {
                entity.name = body.name
            }
            if (body.brand) {
                entity.brand = body.brand
            }
            if (body.heading && body.heading.title) {
                entity.heading.title = body.heading.title
            }
            if (body.heading && body.heading.description) {
                entity.heading.description = body.heading.description
            }
            if (!entity.dimensions[0]) {
                if (body.dimensions && body.dimensions.value) {
                    entity.dimensions[0] = body.dimensions
                }
            } else {
                if (body.dimensions && body.dimensions.value) {
                    entity.dimensions[0].value = body.dimensions.value
                }
                if (body.dimensions && body.dimensions.unit) {
                    entity.dimensions[0].unit = body.dimensions.unit
                }
            }
            if (body.price && body.price.value) {
                entity.price.value = body.price.value
            }
            if (body.price && body.price.currency) {
                entity.price.currency = body.price.currency
            }
            if (body.status) {
                entity.status = body.status
            }
            if (body.isAdded) {
                entity.isAdded = body.isAdded
            }


        }

        return entity
    } catch (err) {
        throw new Error(err)
    }
}


const create = async (body, context) => {
    const log = context.logger.start(`services/group`)
    try {
        let group
        let category = await categoryService.getById(body.categoryId, context);
        let type = await categoryService.getById(body.typeId, context);
        if (!category) {
            throw new Error("categoryId did not match");
        }
        if (!type) {
            throw new Error("typeId did not match");
        }
        var _group = await db.group.find({
            'link': {
                $eq: body.link
            }
        });
        if (_group && _group.length > 0) {
            throw new Error("group link already exists");
        }
       let data = await linkPreview.getLinkPreview(body.link);
       if (data && data.title && data.siteName) {
        body.name = data.title;
        body.imageUrl = data.images[0];
        group = await new db.group(body).save();
       }else{
        throw new Error("group link not valid");

       }
       
        log.end()
        return group
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const getByCategory = async (req, context) => {
    const log = context.logger.start(`services/getByCategory`)
    try {
        const params = req.query;
        let group;

        if (params.categoryId) {
            // group = await db.group.find({
            //     'categoryId': {
            //         $eq: params.categoryId
            //     },
            //     'typeId': {
            //         $eq: params.typeId
            //     }
            // }).sort({
            //     timeStamp: -1
            // })

            group = await db.group.aggregate([

                { $match: { categoryId: params.categoryId } },
                { $group: { _id: { typeName: "$typeName", _id: "$typeId" }, groups: { $push: "$$ROOT" } } }

            ]);
        } else if (params.typeId) {


            group = await db.group.aggregate([

                { $match: { typeId: params.typeId } },
                { $group: { _id: { categoryName: "$categoryName", _id: "$categoryId" }, groups: { $push: "$$ROOT" } } }

            ]);
            console.log(group)
            // find products

        } else {
            group = await db.group.find({}).sort({
                timeStamp: -1
            })
        }
        log.end()
        return group
    } catch (err) {
        throw new Error(err)
    }
}

const getById = async (id, context) => {
    const log = context.logger.start(`services/getByCategory`)
    try {
        const group = await db.group.findById(id)
        log.end()
        return group
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const deletegroup = async (id, context, res) => {
    const log = context.logger.start(`services/deletegroup`)
    let group
    group = await db.group.findById(id)
    log.end()
    if (group) {
        await db.group.deleteOne({ _id: id })
    }
    res.message = 'group deleted successfully'
    return res.message

}

exports.create = create
exports.getByCategory = getByCategory
exports.getById = getById
exports.deletegroup = deletegroup
