'use strict'

const response = require('../exchange/response')

exports.canCreate = (req, res, next) => {
    if (!req.body.typeId) {
        response.failure(res, 'typeId is required')
    }
    if (!req.body.typeName) {
        response.failure(res, 'typeName is required')
    }
    if (!req.body.categoryId) {
        response.failure(res, 'categoryId is required')
    }
    if (!req.body.categoryName) {
        response.failure(res, 'categoryName is required')
    }
    if (!req.body.link) {
        response.failure(res, 'link is required')
    }
    if (!req.body.admin) {
        response.failure(res, 'admin is required')
    }
 
    return next()
}
exports.verifyUser = (req, res, next) => {
    if (!req.body.email) {
        response.failure(res, 'Email is required')
    }
    if (!req.body.otp) {
        response.failure(res, 'Otp is required')
    }
    return next()
}

exports.getById = (req, res, next) => {

    if (!req.params && !req.params.id) {

        return response.failure(res, 'id is required')
    }

    return next()
}

exports.update = (req, res, next) => {
    if (!req.params && !req.params.id) {
        return response.failure(res, 'id is required')

    }
    return next()
}

exports.login = (req, res, next) => {
    if (!req.body.email && !req.body.phone) {
        response.failure(res, 'Email or Phone number is required')
    }
    if (!req.body.password) {
        response.failure(res, 'Password  is required')
    }
    return next()
}

exports.changePassword = (req, res, next) => {
    if (!req.body.password) {
        response.failure(res, 'Password is required')
    }
    if (!req.body.newPassword) {
        response.failure(res, 'NewPassword is required')
    }

    return next()
}
exports.forgotPassword = (req, res, next) => {
    if (!req.body.email) {
        response.failure(res, 'Email is required')
    }
    return next()
}
exports.resetPassword = (req, res, next) => {
    if (!req.body.otp) {
        response.failure(res, 'otp is required')
    }
    if (!req.body.newPassword) {
        response.failure(res, 'new password is required')
    }
    return next()
}