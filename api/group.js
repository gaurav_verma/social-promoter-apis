'use strict'

const response = require('../exchange/response')
const service = require('../services/group')


exports.create = async (req, res) => {
    const log = req.context.logger.start(`api/group`)
    try {
        const group = await service.create(req.body, req.context)
        log.end()
        return response.data(res, group)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.getByCategory = async (req, res) => {
    const log = req.context.logger.start(`api/group/getByCategory`)
    try {
        const group = await service.getByCategory(req, req.context)
        log.end()
        return response.data(res, group)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.getById = async (req, res) => {
    const log = req.context.logger.start(`api/group/getById/${req.params.id}`)
    try {
        const group = await service.getById(req.params.id, req.context)
        log.end()
        return response.data(res, group)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.deleteImage = async (req, res) => {
    const log = req.context.logger.start(`api/group/deleteImage/${req.params.id}`)
    try {
        const group = await service.deleteImage(req.params.id, req.context,res)
        log.end()
        return response.data(res, group)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}


