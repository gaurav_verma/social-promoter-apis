'use strict'

const response = require('../exchange/response')
const service = require('../services/images')


exports.create = async (req, res) => {
    const log = req.context.logger.start(`api/images`)
    try {
        const images = await service.create(req.body, req.context)
        log.end()
        return response.data(res, images)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.getByCategory = async (req, res) => {
    const log = req.context.logger.start(`api/images/getByCategory`)
    try {
        const images = await service.getByCategory(req, req.context)
        log.end()
        return response.data(res, images)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.getById = async (req, res) => {
    const log = req.context.logger.start(`api/images/getById/${req.params.id}`)
    try {
        const images = await service.getById(req.params.id, req.context)
        log.end()
        return response.data(res, images)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.deleteImage = async (req, res) => {
    const log = req.context.logger.start(`api/images/deleteImage/${req.params.id}`)
    try {
        const image = await service.deleteImage(req.params.id, req.context,res)
        log.end()
        return response.data(res, image)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}


