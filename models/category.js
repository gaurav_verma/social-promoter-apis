'use strict'

module.exports = {
    name: String,
    type: {
        type: String,
        default: "category",
        enum: ["category", "type"],
    }
    // quoteImages: [{
    //     _id: String,
    //     name: String,
    //     image: {
    //         url: String,
    //         thumbnail: String,
    //         resize_url: String,
    //         resize_thumbnail: String,
    //     }

    // }]
}