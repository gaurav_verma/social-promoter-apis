'use strict'

// images Module
module.exports = {
    categoryId: String,
    categoryName: String,
    name: {
        type: String,
        lowercase: true,
        trim: true
    },
    isTrending: {
        type: Boolean,
        default: false
    },
    admin: String,
    link: String,
    typeId: String,
    typeName: String,
    imageUrl:String,
    reportCount: {
        type: Number,
        default: 0
    },
    status: {
        type: String,
        default: "active",
        enum: ["active", "blocked", "inActive"],
    }

}