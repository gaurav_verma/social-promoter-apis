'use strict'

// images Module
module.exports = {
    category: {
        _id: String
    },
    name: String,
    image: {
        url: String,
        thumbnail: String,
        resize_url: String,
        resize_thumbnail: String,
    }
}