'use strict'

// User Module
module.exports = {
    type: {
        type: String,
        default: 'user',
        enum: ['user', 'admin']
    },
    status: {
        type: String,
        default: "active",
        enum: ["active", "blocked", "inActive"],
    },
    loginType: {
        type: String,
        default: 'google',
        enum: ['google', 'app']
    },
    name: {
        type: String,
        lowercase: true,
        trim: true
    },
    email: {
        type: String,
        lowercase: true,
        trim: true
    },

    token: String,
    fcmToken: String,
    profileImage: String,
    uId: String,
    pushToken: String


}


// tempToken: String,
// regToken:String,
